var configuracion_nombre="";
var configuracion_emocion=0;

function manejador(elEvento) {
  var evento = elEvento || window.event;
  console.log("["+evento.type+"] El código de la tecla pulsada es " + evento.keyCode);
 
  if($('#div_botonera').is(':visible')){
    if(evento.keyCode==49){
      configurarBienvenida();
    }else if(evento.keyCode==50){
      comenzar();
    }
  }
 
  if($('#div_configurar_bienvenida').is(':visible')){
    $("#mensaje_div_configurar_bienvenida").hide();
    if(evento.keyCode==50){
      var nombre= $('#input_nombre').val();
      nombre = nombre.replace(/[0-9]/g,'');
      $('#input_nombre').val(nombre);
      if(nombre.length>=3){
        seleccionEmocion();
      }else{
        $("#mensaje_div_configurar_bienvenida").show();
        $("#mensaje_div_configurar_bienvenida").html('Por favor verificar el nombre antes de pasar a la siguiente etapa');
        
      }
      
    }
  }

  if($('#div_configurar_emocion').is(':visible')){
    $("#mensaje_div_configurar_bienvenida").hide();
    if(evento.keyCode==49){
      //key 1 -> Buscan en banco de imagenes Alegria
      
    }
    if(evento.keyCode==50){
      //key 2 -> Buscan en banco de imagenes Enojo
    
    }
    if(evento.keyCode==51){
      //key 3 -> Buscan en banco de imagenes Tristeza
    
    }
    if(evento.keyCode==52){
      //key 4 -> Buscan en banco de imagenes Miedo

    }
    if(evento.keyCode==53){
      //key 5 -> Buscan en banco de imagenes Neutral

    }
  }


  
}

function cargando(){
  $('#div_configurar_bienvenida').hide();
  $('#div_configurar_emocion').hide();
  $('#input_nombre').val('');
  $("#mensaje_div_configurar_bienvenida").hide();
  document.onkeyup = manejador;
}

function configurarBienvenida(){
  $('#div_botonera').hide();
  $('#div_configurar_bienvenida').show();
  $("#input_nombre").focus();
  
}

function comenzar(){
  console.log('comenzar');
  $('#div_botonera').hide();
}

function seleccionEmocion(){
  $('#div_botonera').hide();
  $('#div_configurar_bienvenida').hide();
  $('#div_configurar_emocion').show();

}

